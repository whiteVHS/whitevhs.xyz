---
layout: post
title: VGFM archives, requests and more
category: articles
tags: ["music", "vgfm"]
date: 2023-04-14
permalink: /articles/2023/04/14/vgfm-archives-requests/index.html
---

Each weekly broadcast of [VirtGal FM](https://virtualgalleria.net/vgfm) will be archived on the [virtualgalleria Odysee page](https://odysee.com/@virtualgalleria:5) so they'll be accessible. The show itself will also be streamed on Odysee. Tune in **every Tuesday at 10PM UTC**.
<br><br>
To request a song, or if you have an 88x31 button you'd like displayed on the show, send an email to **vgfm at virtualgalleria dot net**.

-H
