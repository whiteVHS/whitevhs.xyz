---
layout: post
title: Tangling files in Emacs Org Mode
category: articles
tags: ["emacs", "foss", "tech"]
date: 2023-08-21
permalink: /articles/2023/08/21/emacs-org-mode/index.html
---

Recently I started dabbling in using [Org Mode for Emacs](https://orgmode.org/), specifically for the use of the Tangle feature. I'm using it as a way to make my configuration files much more legible. It essentially allows you to have both the documentation and the source code combined into one file, and you only have to maintain the one file instead of two separate ones.
<br><br>
For example, I use awesomeWM, which uses a configuration file titled "[rc.lua](https://gitlab.com/whiteVHS/dotfiles/-/blob/main/.config/awesome/rc.lua)". What I did was took all the lua code from that file and split it up into source code blocks with titles and descriptions in a file titled "[rc.org](https://gitlab.com/whiteVHS/dotfiles/-/blob/main/.config/awesome/rc.org)". Inside my "rc.org" is a header argument stating that I want to tangle to the file "rc.lua". What this means is that the source code blocks inside of "rc.org" output to the actual "rc.lua" config file for awesomeWM, which means any changes made to the code blocks in the org document will transfer over to the original file (as long as you run :org-babel-tangle).
<br><br>
One thing I love about org documents is the table of contents - for each header and subheader you add to the document, you can create a table of contents that will automatically add them. If you frequently edit a config file and it's rather lengthy, having it laid out in an org document will help a lot.
<br><br>
I'll most likely end up converting more of my configs to org documents, like my awesomeWM theme configs, or even some scripts of mine. I'll be using org mode a lot more from now on, that's for sure. If it sounds like something you'd be interested in, I highly recommend giving it a try.

-H
