---
layout: post
title: KAARBS version bump (v0.7)
category: articles
tags: ["bash scripts", "cli", "foss", "kaarbs", "linux", "tech"]
date: 2023-03-30
permalink: /articles/2023/03/30/kaarbs-version-bump/index.html
---
My [post-installation script](/kaarbs) for [Arch Linux](https://archlinux.org) went through a complete rewrite, with the following changes:
<br><br>
- Whiptail dialog boxes for an easier end-user experience.
- More packages (vim, airshipper, ranger, btop, unimatrix, you-get, lobster).
- Dmenu is no longer used in favor of Rofi, however it is kept installed for those who want to continue using it.
- Custom rofi themes.
- Removed AUR version of Mupen64Plus.
- Changed ani-cli source from GitHub to AUR.
<br><br>
[KAARBS](/kaarbs) can be ran from a minimal archlinux base (kernel+coreutils+gpu drivers+xorg) to set up a functioning system, or on a pre-existing Arch installation to have a streamlined method of obtaining the packages included in the script.
<br><br>
EDIT: There is now a [KAARBS manual](https://kaarbs.xyz/manual) - use this as a general guide on how to use and modify KAARBS.

-H
