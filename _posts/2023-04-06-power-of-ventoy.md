---
layout: post
title: The Power of Ventoy
category: articles
tags: ["foss", "tech"]
date: 2023-04-06
permalink: /articles/2023/04/06/power-of-ventoy/index.html
---

I'm the kind of person that has multiple USB drives kickin' around for various use cases. One of them typically contains the latest Arch ISO, and another the latest Linux Mint ISO. Only having one ISO per USB drive meant that I'm limited to the amount of physical drives I have. There was also a lot of leftover, unusable space on those drives which always kinda bothered me. Then I discovered there was a solution to my problem - Ventoy.
<br><br>
With Ventoy, you can store multiple ISOs on one USB drive and choose which one to boot into from a menu. You can also use the rest of the drive's space for storage, which is a huge plus for me. There are several [Ventoy plugins](https://www.ventoy.net/en/plugin.html) for configuration/customization, but I have yet to look into them. On my Ventoy drive, I have 5 different ISOs I can boot into - before, that would've required 5 separate USB drives! And there's still *usable* space leftover on the drive for storage. 
<br><br>
For more information, check the [Ventoy website](https://www.ventoy.net). To download, check the [Ventoy Github](https://github.com/ventoy/Ventoy/releases).

-H
