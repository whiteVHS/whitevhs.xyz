---
layout: post
title: Simple mpd+ncmpcpp setup tutorial
category: articles
tags: ["cli", "foss", "linux", "tech", "tutorial"]
date: 2022-11-23
permalink: /articles/2022/11/23/simple-mpd-ncmpcpp-tutorial/index.html
---

### Disclaimer:
This tutorial does not cover any advanced features
of mpd including streaming over a network. This is to help set up a local instance of mpd for a single user.
<br><br>
### Assumptions:

- Your distro's default repository ships with both mpd and ncmpcpp.
(or you are able/willing to compile from source)

- Your current audio server is Pipewire (run `pactl info` to confirm)

- You're using SystemD as your init system
<br><br>
### References:
- [My mpd.conf](https://github.com/basedghost/dotfiles/blob/main/.config/mpd/mpd.conf)

- For [more documentation on how to use mpd](https://mpd.readthedocs.io/en/latest/user.html)

- For [more information on ncmpcpp](https://rybczak.net/ncmpcpp/)
<br><br>
# Install required packages:

First you'll need to install both mpd and ncmpcpp with your distros main package manager.
Since I am on Arch Linux, I just have to run:
<br><br>
`sudo pacman -S mpd ncmpcpp`
<br><br>
If for some reason they're unavailable, you may have to compile them manually.
Additionally, you can install `mpc` to control mpd via command-line.
<br><br>
ex: `mpc volume -2`, `mpc random on`, etc.
<br><br>
This is useful for mapping mpd controls to keybindings.
<br><br>
# Configure mpd
<br><br>
Now that mpd is installed, you'll want to configure it.
The system-wide config file for mpd can be found in /etc/mpd.conf.
Since we'll be running it as a user daemon, the config file
must reside in your home directory. (specifically ~/.config/mpd/mpd.conf)
First, make a directory for the mpd config:
<br><br>
`mkdir ~/.config/mpd/`
<br><br>
Then copy the config file over.
<br><br>
`sudo cp /etc/mpd.conf ~/.config/mpd/mpd.conf`
<br><br>
Now open that file in your favorite text editor
(for simplicity sake I'm using nano in this example):
<br><br>
`nano ~/.config/mpd/mpd.conf`
<br><br>
Make sure your music and playlist directories are set correctly.
<br><br>
```
music_directory    "~/Music"
playlist_directory "~/mpd-playlists"
```
<br><br>
The next thing you'll want to do is find the list of audio outputs in the config and add the following:
<br><br>
```
audio_output {
    type         "pipewire"
    name         "Pipewire Multimedia Service"
}
```
<br><br>
This creates an audio output for pipewire. Feel free to name it whatever you'd like.
<br><br>
![ncmpcpp visualizer](https://whitevhs.xyz/photos/posts/ncmpcpp.png){: style="display:block;margin:auto;width:70%;"}
<br><br>
ncmpcpp has this really neat music visualizer feature.
For that, you'll need to add this as an output:
<br><br>
```
audio_output {
    type                    "fifo"
    name                    "Visualizer"
    path                    "/tmp/mpd.fifo"
    format                  "44100:16:2"
}
```
<br><br>
That should be it for configuring mpd.
Now to start it!
<br><br>
# Starting/stopping mpd
<br><br>
You can start mpd (as a user daemon) with the following command:
<br><br>
`systemctl --user start mpd`
<br><br>
If you'd like mpd to autostart at login:
<br><br>
`systemctl --user enable mpd`
<br><br>
If you'd like to stop mpd:
<br><br>
`systemctl --user stop mpd`
<br><br>
Once mpd is started, open a terminal and run:
`ncmpcpp`
<br><br>
Voila! You now have mpd running with ncmpcpp as a front-end for it.
To show the keybindings for ncmpcpp, press F1.
<br><br>
This may be a simple tutorial, but I hope it can prove to be useful!

-H
