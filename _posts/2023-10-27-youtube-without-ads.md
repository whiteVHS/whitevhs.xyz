---
layout: post
title: Watch YouTube Without Ads
category: articles
tags: ["agora road travelogue", "cli", "foss", "privacy", "tech", "web"]
date: 2023-10-27
permalink: /articles/2023/10/27/youtube-without-ads/index.html
---
If you're one of the many people who still visit YouTube dot com frequently, you may have noticed they began cracking down harder on adblocking software lately. Although it can be ignored, you don't have to put up with it. In this article, I'll show you a few different ways you can watch YouTube without ads, trackers, popups etc.
<br><br>
## Alternative Frontends
<br><br>
### Invidious
Use an [Invidious](https://invidious.io/) instance like [Yewtube](https://yewtu.be) or [Puffyan](https://vid.puffyan.us/). If you're on a mobile device, use [Newpipe](https://newpipe.net/). These provide you with ways to watch YouTube videos without ads or trackers, most instances use proxies to fetch videos, and Javascript is not required (for Invidious). You can create accounts on these, however I explain later how you can keep track of your subs without one.
<br><br>
### Ytfzf
You can watch YouTube videos from the command-line with [ytfzf](https://github.com/pystardust/ytfzf). It allows for downloading videos (with yt-dlp), bypassing age-restrictions on videos, and it can even search [Odysee](https://odysee.com/), which is another video hosting website (which will [most likely disappear](https://odysee.com/@lbry:3f/theendoflbryinc:d) soon sadly). This has been my go-to method for watching YouTube, as it blocks all ads, trackers, and is less bloated than having a browser open. It's especially useful on older hardware.
<br><br>
### EDIT: Browser Extensions
There are browser extensions out there like [LibRedirect](https://libredirect.github.io/) that will automatically redirect links to popular sites to their privacy respecting alternative frontends. This can be useful if you plan on using [Invidious](https://invidious.io/) or [Piped](https://piped.video/). You can also use [SponsorBlock](https://sponsor.ajay.app/) to skip sponsored segments in videos.
<br><br>
Without your YouTube/Google account you might be wondering "How will I keep track of all my subs?". This is where **RSS** comes into play.
<br><br>
## "Subscribe" with RSS
To keep track of the channels I frequent, I subscribe to them via [RSS](https://rss.com/blog/how-do-rss-feeds-work/), which pulls the latest videos for me into my RSS feed so I don't have to check each channel manually. I use the [Newsboat](https://newsboat.org/) RSS reader. If a GUI is more your thing there's [Newsflash](https://apps.gnome.org/en/NewsFlash/). If you're going to use one, I recommend one that allows for offline/local feeds and is [free/open-source](https://fsfe.org/freesoftware/). RSS is also incredibly useful for keeping up with other sites online, but I'll save that for another day.
<br><br>
One thing you should keep in mind with these methods is you will not be training an algorithm to feed you personalized content. The most you'll get is related videos being recommended, but it will not be like normal YouTube. Even though Google has been increasingly more Draconian with adblockers, you don't have to be subject to it. There are ways around their doings, at least for now. When they finally put the axe to the methods aforementioned, I'll be ridding myself of it entirely.
-H
<br><br>
This post was part of [Agora Road Travelogue](https://forum.agoraroad.com/index.php?threads/agora-road-travelogue-october-blogging.6113/) for the month of October.

 


