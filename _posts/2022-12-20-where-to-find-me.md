---
layout: post
title: Where to find me
category: articles
tag: tech
date: 2022-12-20
permalink: /articles/2022/12/20/where-to-find-me/index.html
---

Today I was introduced to a simple but neat and surprisingly purposeful website, aptly named [Where to find me](https://wheretofind.me/).
<br><br>
Their main page pretty much covers everything, I encourage you to check it out!
<br><br>
With my online presence being somewhat minimal, it's crucial that people know *where to find me!*
<br><br>
(Which is [here](https://wheretofind.me/@whiteVHS), btw)

-H

