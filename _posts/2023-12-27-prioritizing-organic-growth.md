---
layout: post
title: Prioritizing Organic Growth
category: articles
tags: ["email", "rant", "web"]
date: 2023-12-27
permalink: /articles/2023/12/27/prioritizing-organic-growth/index.html
---
I received a rather interesting (spam) email today:
<br><br>
![A spam email.](https://whitevhs.xyz/photos/posts/email.png){:width="100%"}
<br><br>
I'll admit it got a good laugh out of me, but the more I thought about it the more infuriated I became. I'm aware this email is 99% likely auto-generated, but if it's not they *clearly* didn't do any actual "reviewing" of my site. My [contact page](https://whitevhs.xyz/contact) explicitly states that I am not on convential social media. Even if I was an avid social media user, and wanted more eyes on my material, why would I consider *paying for artificial engagement?* What benefits would I truly be receiving by having artificially inflated numbers? Why do some people choose to chase meaningless numbers instead of focusing on genuine, organic growth and engagement?
<br><br>
To me it's important to prioritize garnering the [right kind of attention](https://robinrendle.com/notes/the-right-kind-of-attention/) rather than trying to appeal to everybody. In fact, this email has me considering [blocking social media referrals](https://bikobatanari.art/posts/2023/blocking-referrals) altogether. These corporate platforms and the people still using them are actively contributing to the deterioration of the quality and integrity of the world wide web, and I want absolutely nothing to do with it. I'd much rather have next to no visitors on my site finding it organically over paying to have it plastered everywhere just to attract people who won't even appreciate it to begin with.

-H