---
layout: post
title: Reflection and Positive Internalization
category: articles
tags: ["agora road travelogue", "indieweb carnival", "memories", "rant", "thoughts"]
date: 2024-01-29
permalink: /articles/2024/01/29/the-little-things/index.html
---
Inspired by and written for the [IndieWeb Carnival](https://indieweb.org/indieweb-carnival) for January 2024 hosted by none other than [foreverliketh.is / Eden](https://foreverliketh.is/); this post is gonna be dedicated to exploring the topic of [positive internalization](https://foreverliketh.is/blog/indieweb-carnival-january-2024-positive-internalization/). It's also part of the [Agora Road Travelogue](https://forum.agoraroad.com/index.php?threads/agora-road-travelogue-january-24.6313) for January 2024.
<br><br>
*"Your entry for this month’s carnival will have you intentionally seek out positive memories. Memories that remind you of the good parts of yourself; the facets of your being that you want to see more of, that you wish to nurture and grow. By reliving them and seeing yourself in that positive light, you help to internalize a healthier, and more accurate, self-perception."* - [Eden / foreverlikethis](https://foreverliketh.is/)
<br><br>
This post might be all over the place - so please, bear with me.
<br><br>
When I first started playing drums, I wasn't even tall enough to reach the kick pedal, though believe it or not I could play *fairly* well (I mean, as well as any toddler). One big problem arose when I finally became tall enough to reach it - actually learning to incorporate it into my drumming. It's something I truly struggled with; riding my bike without training wheels was a piece of cake in comparison, and I nearly gave drums up because of it... but I didn't. I committed to figuring it out and pulled through. This kind of perserverance, discipline and willingness to try, try, and try again has been deeply rooted in my personality. The recreational activities I indulge in reflect this; let me explain:
<br><br>
A lot of what I do requires tons of patience, trial and error, and an ability to be open and vulnerable; Playing instruments, skateboarding, or tinkering with free and open-source tech, like writing shell scripts or learning about various programming languages. Heck, I even dabbled in learning to speedrun my favorite game for a bit. Take skateboarding for instance; I love skateboarding... but it's not easy. A good chunk of my youth was dedicated to becoming comfortable on a board, riding around, then eventually after *years* of consistently skating, I began learning tricks. Man, nothing beats the feeling of pouring so much time and energy into one thing for it to *finally* come to fruition. Now I can cruise around with ease, hit slappy front 50's and tre flips all day long. It's most certainly delayed gratification. It's also not something you can buy or inherit. These skills are something you need to put your all into.
<br><br>
When it comes to the "good parts of myself" that I can think of:
<br><br>
- I enjoy learning new things, and am always open to feedback, criticism, etc.
- Humans are made to create. I appreciate the arduous process of creation.
- I'm a generally optimistic fella. I tend to focus on positives, silver linings, etc.
- I like solving problems... at least ones that don't elude me, heh.
- Improvement and progression in some form or fashion. I need to be doing something that I can continually learn from.
- One could say I "march to the beat of my own drum", so to speak. I like doing my own thing.
<br><br>

Reflecting on the positive aspects of myself has been surprisingly difficult; it took some deep focus to dig into my memories and correlate them to ways in which I've internalized positive behaviours and traits. It was a fun exercise, though; I enjoyed the experience and I recommend you try it, just for fun. Hopefully you enjoyed reading about it too!

-H

<br><br>
Check out some of this month's [other entries!](https://foreverliketh.is/blog/indieweb-carnival-january-2024-roundup/)
