---
layout: post
title: DJ THINKPAD
date: 2024-03-26 12:00:00
category: microblog
tags: ["microblog", "photos"]
permalink: /articles/2024/03/26/dj-thinkpad/index.html
---
![Thinkpad T500 with Numark DJ2Go2 and Akai MPK Mini 3](/microblog/img/dj.jpg){: style="display:block;margin:auto;width:70%"}
<br>
I managed to get [Mixxx](https://mixxx.org) running flawlessly on my dual-core Thinkpad T500 from 2009! This in combination with the Numark DJ-2-Go2 and the Akai MPK Mini MkIII will be my setup for future shows.