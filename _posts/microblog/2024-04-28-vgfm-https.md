---
layout: post
title: VGFM now HTTPS!
date: 2024-04-28 11:15:00
category: microblog
tags: ["microblog", "vgfm"]
permalink: /articles/2024/04/28/vgfm-https/index.html
---
I finally got around to upgrading the connection for [VGFM](https://vgfm.virtualgalleria.net){:target="_blank"}, which can also be streamed right from the [Virtual Galleria homepage](https://www.virtualgalleria.net){:target="_blank"}. I also migrated the stream over to a dedicated server running Debian. It's been an interesting experience, and I'm sure I will have more to share about it in the future.