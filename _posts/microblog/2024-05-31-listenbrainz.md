---
layout: post
title: ListenBrainz
date: 2024-05-31
category: microblog
tags: ["microblog", "music"]
permalink: /articles/2024/05/31/listenbrainz/index.html
---
I signed up for a [ListenBrainz](https://listenbrainz.org/user/whiteVHS/) account the other day to scrobble what music I'm listening to. I have it hooked into MPD on my desktop and my self-hosted [Navidrome](https://navidrome.org/) instance. Pretty neat!