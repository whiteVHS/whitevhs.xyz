---
layout: post
title: Dream Keyboard
date: 2024-03-15 12:02:00
category: microblog
tags: ["microblog", "photos"]
permalink: /articles/2024/03/15/dream-keyboard/index.html
---
**Why** is it that *all* the features I look for in a computer keyboard are so incredibly rare to see together?
<br><br>
I need a TKL because my desk is so small. I'd prefer buckling spring switches and **most importantly** a trackpoint so I don't need to move my hands away from the keyboard to control the mouse. The *closest* thing I could find was the **IBM Space Saver II**:
<br>
![An IBM SpaceSaver II computer keyboard](/microblog/img/ibm_spacesaver_ii.jpg){: style="display:block;margin:auto;width:70%"}
However it does *not* have buckling springs, and costs far too much (around ~$200 CAD). I'd settle for it if I could afford it.

-H


*[TKL]: tenkeyless