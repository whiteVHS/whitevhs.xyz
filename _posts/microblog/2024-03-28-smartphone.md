---
layout: post
title: Smartphone?
date: 2024-03-28 17:10:00
category: microblog
tags: ["microblog", "photos"]
permalink: /articles/2024/03/28/smartphone/index.html
---
![Screenshot of NPC dialogue from SMTIV that reads "Young woman: Smartphones? Not interested. That's those machines with demons in them, right?"](/microblog/img/smartphone_smt4.png){: style="display:block;margin:auto;width:90%"}