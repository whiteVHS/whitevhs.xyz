---
layout: post
title: Billie Holiday - Ghost of Yesterday
date: 2024-03-16 13:38:00
category: microblog
tags: ["microblog", "music"]
permalink: /articles/2024/03/16/ghost-of-yesterday/index.html
---
"Foolish heart must pay, ghost of yesterday"<br><br>
[Billie Holiday - Ghost of Yesterday (Song)](https://redirect.invidious.io/watch?v=r2RZvt0BzaI&listen=1){:target="_blank"}