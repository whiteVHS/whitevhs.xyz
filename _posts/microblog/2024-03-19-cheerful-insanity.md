---
layout: post
title: Giles, Giles and Fripp - The Cheerful Insanity of
date: 2024-03-19
category: microblog
tags: ["microblog", "music"]
permalink: /articles/2024/03/19/cheerful-insanity/index.html
---

[Giles, Giles and Fripp - The Cheerful Insanity of Giles, Giles and Fripp (Album)](https://archive.org/details/the-cheerful-insanity-of-giles-giles-and-fripp-1968){:target="_blank"}
<br><br>
Before there was [King Crimson](https://wikiless.org/wiki/King_Crimson?lang=en){:target="_blank"} there was Giles, Giles and Fripp. Surprisingly not many know about this album, and it's a shame it sold poorly when it debuted. Two of three members (Michael Giles and Robert Fripp) went on to form the first line-up of King Crimson with Greg Lake, Ian McDonald and Peter Sinfield (with Peter Giles appearing on their second album).