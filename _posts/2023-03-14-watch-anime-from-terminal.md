---
layout: post
title: Watch anime from the terminal
category: articles
tags: ["anime", "cli", "foss", "linux", "tech"]
date: 2023-03-14
permalink: /articles/2023/03/14/watch-anime-from-the-terminal/index.html
---
[ani-cli](https://github.com/pystardust/ani-cli) is an amazing command-line utility which allows you to stream or download anime straight from your terminal. Here are some practical reasons why you'd want to do so:
<br><br>
#### 1) Modern Web Bloat!
Avoiding sketchy streaming sites when and where possible is always a plus. Using `ani-cli` to watch anime uses next to no system resources. Even my 14 year-old Thinkpad can run it like a dream.
<br><br>
#### 2) Efficiency!
Say you're trying to watch episode 12 of Hyouka. It's much faster to type `ani-cli hyouka -e 12` in your terminal than it is to open a web browser, go to your preferred streaming site, search it, click the episode, and click play.
P.S. - You can set an alias for `ani-cli` in your `~/.bashrc` to something like `alias anime='ani-cli'`.
<br><br>
#### 3) Stream OR download!
Using the -d flag allows you to download episodes, even multiple at a time which is great for offline play/use *without* having to torrent.
<br><br>
#### 4) Quickly start where you left off!
The -c flag brings you back to your last played anime, allowing you to quickly jump back to where you last were.
<br><br>
The download and installation steps for `ani-cli` are available [here](https://github.com/pystardust/ani-cli).
Enjoy this great piece of software! :)

-H
