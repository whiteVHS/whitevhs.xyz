---
layout: post
title: Join my Matrix Space
category: articles
tags: ["foss", "matrix", "tech"]
date: 2022-12-17
permalink: /articles/2022/12/17/join-my-matrix-space/index.html
---

[Click here](https://matrix.to/#/#mididungeon-space:matrix.org) to join my Matrix space.
<br><br>
Recently, I got back into using [Matrix](https://matrix.org), an open network for secure, decentralized communication. There are several clients you can choose from that use the Matrix protocol - for simplicity sake I chose [Element](https://element.io) - one of, if not, the most popular/most-used client, as well as using the matrix.org homeserver for my account. Element is available as a webapp, as well as desktop and mobile apps. My plan/goal is to use Element as my main messaging application, as the 1:1 conversations use E2EE (end-to-end encryption) by default. I've managed to get a few of my friends and family to try it out (my dad even went as far as to self-host his own homeserver!). Matrix will certainly be the only application I use for direct/private messages.
<br><br>
The interoperability of Matrix allows for the [bridging of multiple platforms](https://matrix.org/bridges), which is incredible and should be the standard, in my opinion.
<br><br>
While my Matrix space does not use E2EE, it is much better than using something like Slack or Discord for group chats (~~although I do have the space bridged with a Discord server, to help ease the transition for myself+others~~). Any sensitive information should be shared in private, end-to-end encrypted chats. E2EE allows you to send messages without the possibility of it reaching the eyes of anybody other than the intended recipient. (However there is always the risk of somebody screenshotting your messages).
<br><br>
One piece of advice I will give to anyone who decides to join - **KEEP YOUR BACKUP RECOVERY KEY SOMEWHERE SAFE** - you do not want to end up locked out of your encrypted chats.
<br><br>
TL;DR - you can [join here](https://matrix.to/#/#mididungeon-space:matrix.org).
<br><br>
EDIT (01.24.2023) I have deleted the discord server, however the Matrix room is still up, and has an encrypted chat as well.

-H
