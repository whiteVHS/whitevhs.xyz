---
layout: post
title: Revive your old PC with "Pi-Hole"
category: articles
tags: ["agora road travelogue", "bash scripts", "cli", "foss", "linux", "pihole", "privacy", "tech", "tutorial"]
date: 2023-09-11
permalink: /articles/2023/09/11/pihole-archlinux/index.html
---
Last updated on 17 Mar 2024
<br><br>
If you happen to have a spare PC or laptop kickin' around collecting dust, you could potentially use it as a [Pi-hole](https://pi-hole.net/) - a network-wide ad and tracker blocker which is typically ran on a Raspberry Pi, hence the name. It acts as a DNS sinkhole, filtering and blocking requests on a network level. It's not a 100% fool-proof solution, but rather another layer of protection (in conjunction with your adblocker). If you set up the Pihole for the entire network, it can be useful for devices that do not typically have content blockers like "smart" TVs, IoT devices, etc. Otherwise you can set it up as opt-in which requires you to be able to assign a custom DNS per device. In this post, I'll briefly go over how I set my Pihole up - you may want to take a different approach. If you do wanna give it a shot, I wrote [this setup script](https://codeberg.org/whiteVHS/pihole-arch) to help speed up the process.
<br><br>
First things first - the OS. I'm sure there are other operating systems you could use to do this, but as I am most comfortable with [Arch Linux](https://archlinux.org/) that is what I'll be using. You'll want to burn an Arch Linux ISO onto a USB drive and boot into it on the device you wanna use as your Pihole. For the sake of time and convenience, run through the `archinstall` script and choose the minimal profile and NetworkManager for networking. This will quickly set up a base system for you. From there, you can use [my setup script](https://codeberg.org/whiteVHS/pihole-arch):
<br><br>
{: .language-bash}
```
#!/usr/bin/env bash
sudo pacman --needed -S git sed btop;
cd;
git clone https://aur.archlinux.org/yay-bin.git;
cd yay-bin;
makepkg -si;
cd;
rm -rf yay-bin;
yay -S pi-hole-server;
systemctl enable pihole-FTL &&
systemctl start pihole-FTL &&
sudo pacman -S php-sqlite php-cgi lighttpd;
sudo sed -i '/extension=pdo_sqlite/s/^;//g' /etc/php/php.ini &&
sudo sed -i '/extension=sockets/s/^;//g' /etc/php/php.ini &&
sudo sed -i '/extension=sqlite3/s/^;//g' /etc/php/php.ini &&
sudo cp /usr/share/pihole/configs/lighttpd.example.conf /etc/lighttpd/lighttpd.conf &&
systemctl enable lighttpd &&
systemctl start lighttpd &&
echo -e "Pi-hole is now installed! :)\nVisit https://wiki.archlinux.org/title/Pi-hole for more information."
```
{: .language-bash}
<br><br>
If you'd rather do each step yourself: install an AUR helper like paru or yay then [follow this guide on the Arch Wiki](https://wiki.archlinux.org/title/Pi-hole) to install and configure all the necessary packages for the Pihole to operate. Once that's done and everything is set up, you'll have to access your routers settings from another computer. You'll want to set the local IPv4 address of the pihole to be reserved/static (NOT dynamic). The reason for this is simple - anytime that address changes, you'll have to manually change it on each device you have connected to the Pihole, so ideally you'll want this IPv4 address to stay consistent. This will also help for remoting into the Pihole via [SSH](https://wiki.archlinux.org/title/OpenSSH) (which I will not be going over, but I highly recommend setting up).
<br><br>
If you want to use the Pihole for your entire network, set your routers primary DNS to the local IPv4 address that's assigned to your Pihole (ex: 192.168.1.120). If the Pihole goes offline or needs to reboot after an update, for good measure I set the Quad9 DNS servers as my backups: `9.9.9.9` and `149.112.112.112`
<br><br>
**NOTE:** Some routers, namely ones provided by ISPs may not allow you to do this. In this case I recommend buying a separate router.
<br><br>
If, for any reason, you do not want the Pihole to be active for your entire network but rather on a per-device basis, you still need to make sure the local IPv4 address of the Pihole is static. Then, on the device you want to route through the Pihole, go into the Network settings and if you can set a custom DNS server, add the Pihole's local IPv4 address.
<br><br>
By default the Pihole uses Google DNS, so I switched it to [Quad9](https://www.quad9.net/) which is a free, privacy respecting DNS service. This is an optional step, but I recommend it if you value your privacy. You can do this via the web interface, which you can access by putting the local IPv4 address of your Pihole into your browser followed by `/admin` (ex: http://192.168.2.120/admin).
<br><br>
Currently I'm using an old dual-core HP laptop I had sitting around in a cemetary of laptops from friends/relatives (I have like 5 that just either won't boot or have broken displays). It hadn't been used in years, but still runs great for this use case (in fact it's overkill). Every 2-4 weeks I'm gonna update the OS on it. So far it's been working great. One of my neighbors laptops is routed through and they're running Windows 10 - with a [Windows telemetry blocklist](https://raw.githubusercontent.com/kevle1/Windows-telemetry-blocklist/master/windowsblock.txt) and the [Big OISD list](https://big.oisd.nl/) added to my Pihole, roughly 50% of their requests have been blocked in the last 24 hours (1200 out of 2200). In contrast my PC has sent 6500+ requests in the same amount of time and less than 15 have been blocked. I have approximately 466 thousand domains in my Adlists.
<br><br>
All in all, if you have a spare computer sitting around, a free afternoon and the motivation to tinker, you too could have a Pi-Hole up and running in a small amount of time.

-H
<br><br>
This post was part of [Agora Road Travelogue](https://forum.agoraroad.com/index.php?threads/agora-road-travelouge-september-blogging.6061/) for the month of September.


*[DNS]: Domain Name System
