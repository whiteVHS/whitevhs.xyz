---
layout: post
title: Microwhat?
category: articles
tags: ["updates", "web"]
date: 2024-03-10
permalink: /articles/2024/03/10/microblog/index.html
---
Well, I finally did it. My microblog page is live [here](https://whitevhs.xyz/microblog). Not much there at the moment, but from here on that'll be where my short, informal posts will reside. There's an [Atom/RSS feed](https://whitevhs.xyz/microblog/atom.xml) for it too so you can keep up with it easily.
<br><br>
Well, that's all for now. Happy hacking, everyone.

-H