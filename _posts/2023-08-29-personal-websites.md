---
layout: post
title: The Importance of Personal Websites
category: articles
tags: ["thoughts", "web"]
date: 2023-08-29
permalink: /articles/2023/08/29/personal-websites/index.html
---

My site has been up for just over a year now, and in that time I've encountered so many interesting websites, all of them respectively created and maintained by just one person in their free time. Most of them share the same sentiment that having a personal website is liberating, and can be a great way of getting your thoughts and ideas out. Having a place to properly and freely express yourself is critical. Most Big Tech platforms can and will ban whoever they feel like without a moment's notice, and without reason. After all, it's in their [Terms of Service](https://tosdr.org). Most of them also require you to use your real identity. You should have the choice to remain anonymous online. While I do believe more people should have their own website, at the same time I understand that it's not for everyone. If you want to carve out your own space online that you can *truly* call home, consider creating your own website(s). But before you do, you might ask yourself:
<br><br>
## Where do I start?
This is a great question, but the answer will be different for everyone, as it really depends on what you want to do with your site. I highly recommend checking out [this thread](https://forum.agoraroad.com/index.php?threads/the-personal-web.5647/) on Agora Road; it's what prompted me to write this in the first place. Included in the thread is a list of ways you can start working on your site, which I implore you to peruse through if you're truly interested in creating your own site. In this post I'm gonna break down my workflow for this site.
<br><br>
## The cost
When I first became interested in hosting my own website, I wanted to do so in a way that would cost me the least amount of shekels. I purchased my domain on [Epik](https://epik.com/) for about two dollars. Renewing it does cost more, however (I think it was like $35 to renew it til 2025). Keep in mind, some of the ways you can go about creating your site will NOT require a custom domain, it's more of an optional thing. Anyway, I remember stumbling on a website hosted with Github Pages. Looking into it, I found out that you can easily host a website on there for free. That led me down the path of using a Static Site Generator (or SSG). By default Github Pages uses the Jekyll SSG so that's what I went with. Eventually [I migrated all of my code to Gitlab](https://nogithub.codeberg.page/) because I'd rather not use a Microsoft owned service. I'll admit, having very little experience with this sort of thing meant that my first month had a somewhat steep learning curve, with lots of trial and error. I wanted to do everything by hand, and figuring out how to set everything up was confusing at first, for sure. But the more time I took to figure it out, the more I actually understood what I was doing. Sidenote: I still have no idea what I'm doing.
<br><br>
## The code
Using an SSG means the actual pipeline for your site is handled for you (of course you have to configure it initially, but it's a "set it and forget it" kinda thing). You make your changes, commit it, and boom, within a couple minutes your changes are live. Before I realized I can just press F12, edit the HTML or CSS on the site locally, then apply those changes to the actual codebase, I was making the smallest little changes and committing them. DO NOT MAKE THE SAME MISTAKE AS ME. I feel stupid for not realizing it sooner, but the cleaner you can keep your commits, the better. Using an SSG means you can set up layouts for your page, which is handy for not having to include a bunch of redundant code in your index.html files. For example, everything at the top and bottom of my site is included in the layout (header + footer), and all the actual content of the page is injected via liquid tags.
<br><br>
## The possibilities
The best part about owning your own site, in my opinion, is just the sheer amount of possibilities there are. The biggest limitations you have are your imagination and ability to produce the necessary code. You can turn it into anything you'd like. In all honesty, you don't *have* to write all the code by hand. There are tons of templates and themes out there available for public use, however I personally did not take that route as I wanted to learn how to do it from scratch. Take whatever path you feel is the best for you.
<br><br>
## The downsides
Of course, the method I chose for hosting my site does have its downsides. One of them being the fact that I'm not self-hosting, and technically I am still at the mercy of Gitlab if they so choose to terminate my account. This does pose a potential problem if the kind of content you want to display on your site goes against their terms. Another downside is the fact it is only a static site, and you don't have any access to tools like PHP for backend stuff. You're basically stuck with the limitations that are imposed on you. But hey, it's gratis (free as in "free beer") and so far I've managed just fine. Again, it all comes down to what you'd like to do with your website.
<br><br>
The importance of these independent spaces online cannot be overstated. They're essential for a free and open web. It gives me hope to see that this "Old Web Revival" movement exists to some degree. If you are curious about the personal websites that are out there, check out the [Wiby search engine](https://wiby.me/), or search out the various [Webrings](https://wikiless.org/wiki/Webring) that exist. Have fun creating and exploring.

-H
