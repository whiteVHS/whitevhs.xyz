---
layout: post
title: My Website Workflow
category: articles
tags: ["tech", "updates", "web"]
date: 2024-05-13
permalink: /articles/2024/05/13/website-workflow/index.html
---

I recently migrated all of my webhosting to my own dedicated server, and in that migration was a shift in my workflow as well. First I'll explain my previous workflow, then go into my current one.<br><br>
Just a few weeks ago, all of my websites *and* git repositories were being hosted on [GitLab](https://gitlab.com). Each site had it's own git repo, which I kept a local copy of on my desktop. Any changes to a site would be pushed to their respective git repo and GitLab would handle the rest. While this approach was *very* streamlined, I wanted to actually self-host my websites. Now that I am, the process of pushing changes has become slightly more complicated, but not by much. Let me explain:<br><br>
Say, for example, I make a new post on this website, whitevhs.xyz. First, I actually create the post locally on my desktop, then push the changes to the git repo for the site (which is now hosted by [Codeberg](https://codeberg.org)). Then, I use SSH to remote into the server, pull the changes I made and begin the actual build process for the site. Again, I have made this much simpler by setting aliases per-site, for example, to build a local version of this site I would run `jek-whitevhs`. This allows me to check the website before actually committing the changes (I was **not** able to do this with GitLab hosting my sites). If I am happy with the result, I just run `push-whitevhs` and that copies over all the newly modified files to the webserver directory and restarts the webserver.<br><br>
Basically, I make a change to a site (locally on my PC), SSH into the server, pull the changes, then `jek-domain` to see the website before the changes go live. Finally, `push-domain` makes the changes visible to everyone else.<br><br>
That's about it. Hope you enjoyed reading about my updated website workflow!<br><br>
-H



*[SSH]: Secure Shell
