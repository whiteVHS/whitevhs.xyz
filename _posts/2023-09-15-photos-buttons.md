---
layout: post
title: Recent Site Additions
category: articles
tags: ["updates", "web"]
date: 2023-09-15
permalink: /articles/2023/09/15/photos-buttons/index.html
---
There have been two recent additions to this website - the [web buttons page](https://whitevhs.xyz/buttons), which is a collection of 88x31 buttons from fellow netizens or neat and interesting sites, and my [photography archive](https://whitevhs.xyz/photos/archive) which currently contains 2,000+ photos that I've taken from 2013 to 2022 (I've currently only uploaded up to December of 2017 as of this post). If you're bored or feeling adventurous, check 'em out.

-H