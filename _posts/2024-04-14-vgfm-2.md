---
layout: post
title: VGFM 2.0
date: 2024-04-14
category: articles
tags: ["music", "vgfm"]
permalink: /articles/2024/04/14/vgfm-2/index.html
---
Last updated April 28 2024
<br><br>
[VGFM](https://www.virtualgalleria.net/) is back!
<br><br>
After almost a year of being off the air, VirtGal FM is making a return... but now playing **24/7!** However, this time the broadcasts will no longer be on [Odysee](https://odysee.com/@virtualgalleria:5). To be honest, I was never a fan of Odysee for mulitple reasons; long buffering times, the integration of "Web3" and blockchain technologies, among other things. It was used simply because it was a free streaming/hosting platform. Now I have the stream running on a dedicated server, every day, every night, all the time.
<br><br>
**FUN FACT!** You can open the [stream source URL](https://vgfm.virtualgalleria.net) in a media player like MPV or VLC to get song, artist and album metadata while you listen!
<br><br>
Remember, it's a secret to everybody...
<br><br>
-H



*[MPD]: Music Player Daemon
*[VGFM]: Virt(ual) Gal(leria) Frequency Modulation
