---
layout: post
title: Escaping the Algorithms
date: 2024-02-xx
tags: ["algorithms", "privacy", "security", "tech", "web"]
---
# Table of Contents
1. [Introduction](#introduction)
2. [The Lists](#the-lists)
3. [The Big G](#the-big-g)
4. [Meta? No Thanks.](#meta-no-thanks)
5. [Twitter and Microblogging](#twitter-and-microblogging)
<br><br>
# Introduction
To say [Big Tech](https://wikiless.funami.tech/wiki/Big_Tech?lang=en) has a tight grip on our lives is an understatement; we rely on their services more than any of us would like to admit. We're completely at their whim - surveilling every movement, manipulating what information we can access, swaying public opinion and manufacturing false realities.
<br><br>
By now most people are at least aware of what an [algorithm](https://wikiless.funami.tech/wiki/Algorithm?lang=en) is. In this context, it's what almost every popular tech service or platform is governed by.<br>
A "[black box](https://wikiless.funami.tech/wiki/Black_box?lang=en)"; an all-seeing eye that controls what you see, how much you see and when you see it. Algorithms are everywhere, constantly feeding off one's personal information, interests, private messages, and level of engagement with the platform to provide you with suggestions or recommendations. There's also been a number of major players like [Reddit](https://arstechnica.com/information-technology/2024/02/your-reddit-posts-may-train-ai-models-following-new-60-million-agreement/) and [Tumblr](https://www.businessinsider.com/tumblr-selling-user-data-ai-trianing-openai-midjourney-weird-2024-2), [among others](https://www.vox.com/technology/24086039/reddit-tumblr-wordpress-whos-selling-your-data-to-train-ai), who have recently been in the public eye for allowing their user-generated content to be scraped for "AI" training. To be short, just about **every major tech company** is scraping this data for their own use, without your ["consent"](https://tosdr.org/).
<br><br>
I can *sorta* see why this would be appealing - the more you interact with "content" you like (your work is **more** than just content!), the algorithm will (hopefully) provide you with similar things. Here's the thing though - this entire system is flawed. It's not about what you like anymore, if it *ever* was. **It's about what you will buy and what will keep you on their platform. Period.** That video that made your blood boil? You watched all of it, so now the algorithm takes that as a sign to show you more. This is a side effect of human nature, and how we tend to gravitate towards things that aren't particularly healthy for us. Pair that with an algorithm devoid of any humanity and an incentive to monitize our digital interactions - it's a recipe for disaster.
<br><br>
As an aside, I'd like to talk about how as a user of a Big Tech social media platform, the more time, energy and money you invest into their platform, the more you're helping *them*, not yourself. Think about it: an aspiring musician posts their video to Instagram. It goes viral. You may think this is helping the artist to grow, which in a way it is, but it's also helping Instagram to grow. It's exploitation by Big Tech conglomerates and advertisers to get more time out of your day on their platform and more money out of your pockets and *into theirs*. They **DO NOT** care about you, your privacy or your wellbeing. They **DO** care about extracting as much value from you as possible to their benefit. The difference between posting on your own site/self-hosted service VS a proprietary, Big Tech owned one is **YOU OWN THE DATA**.
<br><br>
One can mitigate a *fair* chunk of their exposure to the algorithmic junk, but it's not simple - it will take lots of planning, time, commitment and reflection. Companies go out of their way to make it difficult for you to [opt-out](https://www.wired.com/story/how-to-stop-your-data-from-being-used-to-train-ai/). You'll never reach 100% detachment from them, unless you completely isolate yourself from society... let's not take such an extreme route and *actually think* about **where** the algorithms you want to avoid are, then (and this is the most important part) **plan and take action** from there. As somebody who had the patience and commitment to detach from the Big Tech shitshow and take the dive into digital spaces that more closely aligned with my values, I want to show others who are curious about escaping the algorithms that they *can* do it! **That's you**, reader. I want to help you on your own personal digital journey. The process of identifying *what* data you want to protect and *who* you want to protect it from is known as ["threat modelling"](https://thenewoil.org/en/guides/prologue/threat-model/).


*[ToS]: Terms of Service
*[AI]: so-called Artificial Intelligence
*[FDSD]: Firewalls Don't Stop Dragons

<br>
~
<br><br>
 This is written for somebody who wants to rid themselves of the constant targeted ads, recommendations and "for you" pages. This is for somebody who can step back and see how big of an issue this is - how much **power** we have all given these algorithms (and by proxy the people who created them) over our beliefs, ideas, attitudes, behaviours and culture. We're losing our ability to curate the **human** way, it's about time we change that.
This is for somebody who wants to distance themselves from the ubiquitous surveillance of these monolithic, manipulative technologies and *reclaim* their digital lives with the use of **user-respecting software**; somebody who has noticed the algorithms and the erosion of privacy have become normalized. For instance, I've seen people bring up how "their phone is listening to them" as if it's something to make jokes about rather than a cause for concern. Google Homes and Amazon Alexas are in millions of homes; voluntarily wiretapping your house is something people gift to others, and owners of these devices often *take pride* in them.
<br><br>
These algorithms **aren't going away** - in fact they're only getting more aggressive, but there *can* be change if people have the awareness to make informed decisions about the software and hardware they use as an individual, and on a grander scale, as communities. I'll get more into detail later about how choosing software that respects the user (i.e Free Software) plays a role in this.
<br><br>
~
<br><br>
As I stated previously, it's crucial that you **personally identify which algorithms/platforms you want to avoid**. This is arguably **the most important step**, as it will determine everything you do moving forward. This type of planning is something I cannot do for you... however, I *can* show you which platforms I chose to eliminate from my life and how I went about doing so. By documenting this information, I aim to create something that can be a resource for others. Rather than defining any strict rules or guidelines, I will simply explain my journey for others to use as a baseline for their own.

<br><br>
# The Lists
Below is a list of services that I have **opted out** of using, a rank of how I recommend to use them (or if you should) and some alternatives, some of which are privacy respecting front-ends that block ads and trackers, and others are entirely separate projects from the original service. I suggest writing your own list of services that you use currently. This can be your way of tracking what you want to eliminate.<br>**Note:** I don't actually *use* all of these (for example, Lemmy). I will break them down in detail further on:

<br><br>
| Service | Avoidance Level | Alternatives |
| ------- | --------------- | ------------ |
| Google Search | Do Not Use | Searx, Startpage, Brave Search
| Gmail, Outlook | Do Not Use | Tuta
| Google Drive | Do Not Use | ???
| Google Maps | Avoid When Possible | OpenStreetMap
| YouTube | Avoid When Possible | Invidious, Peertube
| Amazon | Do Not Use | Other online vendors!
| Twitch | Avoid When Possible | Kick, Owncast
| Facebook | Do Not Use | Mastodon, Pleroma
| Messenger (Facebook) | Do Not Use | Signal
| Discord | Do Not Use | Matrix & Element Call, Nextcloud Talk
| Instagram | Do Not Use | Pixelfed
| Twitter | Do Not Use | Mastodon, Personal Website
| Reddit | Avoid When Possible | Lemmy, Kbin
| Linktree | Do Not Use | WhereToFindMe
| Microsoft Windows | Avoid When Possible | GNU/Linux
| 3rd-Party Sign-ons | Do Not Use | Email/Password combo
<br><br>
Here's a list of services I have never personally used, but are still important to list:

<br><br>
| Service | Avoidance Level | Alternatives |
| ------- | --------------- | ------------ |
| TikTok | Do Not Use | Nope.
| Snapchat | Do Not Use | Your Messenger of Choice
| Spotify, YouTube Music | Do Not Use | Bandcamp + Soulseek and your Music Player of Choice
| 

<br><br>
## The Big G
Google - what a behemoth. With over 80+ services offered by them (in exchange for your privacy) it's no wonder why they're so prevalent, almost impossible to avoid at times. Depending on how many of these services you've become entrenched in will determine how difficult it will be for you to migrate to other services. Fear not! If you are determined to do it, there are logical ways of going about it. I won't go into much detail about it myself, but I will say I highly recommend [this guide on de-googling](https://firewallsdontstopdragons.com/reducing-my-google-footprint/) from Firewalls Don't Stop Dragons, as well as this page rightfully titled [No More Google](https://nomoregoogle.com/). I didn't follow the FDSD guide to a tee myself, but it did lay the foundation for my de-googling endeavour. This is probably going to be the most difficult one for people, as most don't even realize how much of their lives they've let Google claim.
<br><br>
It's crucial that I state **this is not an overnight task**. Take your time and take it one thing at a time.
<br><br>
My personal approach to this went hand-in-hand with setting up a [password manager](https://keepassxc.org/), which involved me making entries for every online account I had. It was during this process that I was able to visually map out just how many accounts I had linked to Google/Gmail, and where I had to swap out my Gmail address for my [ProtonMail](https://proton.me/) address. Also worth mentioning is [SimpleLogin](https://simplelogin.io) which you can use for aliased email addresses. Useful for not giving away your primary email everywhere, but all the mail gets sent to your primary one.
<br><br>
**A WORD OF CAUTION:** to those using 3rd-party logins (e.g using Google to sign up/log in to XYZ website/service) you **will not** be able to change your account credentials. Those accounts are inherently attached. If you delete your Gmail account, or the service you used to sign up with no longer exists you will no longer have access to any accounts you used it for 3rd-party sign-ons. For this reason alone, I always recommend against using them.
<br><br>
## Meta? No Thanks.

## Twitter and Microblogging



