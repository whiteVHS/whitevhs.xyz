---
layout: post
title: GNU/Linux Made Computing Fun (Again)
date: 2024-03-xx
category: articles
tags: ["foss", "linux", "tech"]
---
If it wasn't for the GNU operating system paired with the Linux kernel, I would not have discovered all of the wonderful tools I use on a daily basis. I wouldn't have the granular control over my desktop to tailor it to my needs. I wouldn't have the freedom to write a script to replicate said desktop. I wouldn't have made my own websites, or host a bunch of services from my own server. And most importantly, I most likely wouldn't have thought critically about the ethics and morality surrounding software, and how much of an impact it has on our lives.


NOTES (gotta get deez thoughts out to write)

- endless customization and control
- simple and elegant
- made doing webdev stuff dead simple (thanks git)
- learning how to write shell scripts, navigate cli, vim/emacs, etc.
- always finding cool new software (or writing my own heheh)
- embedded systems (openwrt on linksys router, arkOS on gameboy clone thing (need the name of it))
