---
layout: post
title: Container Tabs
category: articles
date: 2024-01-xx
tags: ["tech", "web"]
---

There's an extension for Firefox-based browsers that I've noticed isn't brought up often - [Container Tabs](https://addons.mozilla.org/en-US/firefox/addon/multi-account-containers/), a way to isolate login credentials, cookies, and sessions in your browser. For example, you could be logged into the same website on separate accounts in separate containers, without the need for a second browser, "private browsing" mode (which truly isn't private) or any of that. This can be extremely useful. Containers will also isolate cookies, which means containers can't share information with each other. This, of course, is how you're able to have multiple logins in separate containers on the same website. It's a great way of keeping your personal/work/bank,etc. logins separate. For example, I made containers specifically for certain sites. If they were to potentially look through the history/cookies, it's the only site I've visited as far as they know.

"Multi-Account Containers", in my opinion, are an underrated feature in Firefox browsers and from what I can tell is a pretty big advantage over it's Chromium competitors. The main browser I use, [LibreWolf](https://librewolf.net) has this functionality by default. Since I found out about container tabs I've been using them extensively, as they provide a convienient method of compartmentalizing your online accounts.

-H
