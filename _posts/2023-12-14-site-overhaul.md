---
layout: post
title: Major Site Overhaul
category: articles
tags: ["updates", "web"]
date: 2023-12-14
permalink: /articles/2023/12/14/site-overhaul/index.html
---
Hey, all! It's been a while since I've posted here. I've been quite busy IRL - not gonna divulge the details, but it's a whole lot of bullshit. It's not all bad though thanks to creative outlets. Working on a couple albums currently, one being released through a net label (more info on that soon!) as well as getting back into the groove of making [digital collages](https://whitevhs.xyz/undo/#art). Went through one hell of a creative drought but we're back now. Anyway, to the topic at hand - I spent the last two days doing a major site overhaul, so now it should look good no matter what device you're on. No, I'm not bending the knee to mobile devices, I just got tired of how inconsistent the layout was even across different computer screen resolutions. It also gave me the opportunity to clean up a lot of junk code I had, and cleaned up the last of the [hotlinked images](https://risingthumb.xyz/Writing/Blog/Avoid_Hotlinking) I had lurking on the site. I even preserved [the old layout](https://whitevhs.xyz/old/) for those who want to experience the jank.

-H