---
layout: post
title: The Current State of Linux Gaming
category: articles
tags: ["agora road travelogue", "foss", "linux", "tech", "vidya"]
date: 2023-10-23
permalink: /articles/2023/10/23/linux-gaming/index.html
---

All aspects of gaming from installation to performance on a GNU/Linux distribution has improved tremendously over the last few years thanks to the efforts of people working on projects like Wine, DXVK, and Proton. Your mileage may vary depending on a multitude of factors: your combination of hardware, the distro you're using and how updated their repos are (specifically for drivers, kernel, etc.), individual game compatibility and performance, among other things. That's not to say you'll have a ton of issues, but it truly does depend on *what* you want to play, and if your hardware is up to snuff.
<br><br>
## What games work?
At this point in the game, the list of playable games outnumbers the unplayable ones, so it'd be easier to list the ones you can't play. Really, the only games that absolutely will not run under Linux are games that include a kernel level anti-cheat (and considering they have access to Ring 0 of the kernel, they're basically rootkits). These kinds of invasive tools were only designed for the Windows NT kernel, and translating these calls to something the Linux kernel can understand is no easy task. This means games like Valorant, recent Call of Duty titles, Destiny 2 and others will never run natively under Linux unless the developers completely scrap or overhaul their anti-cheat systems, which is highly unlikely. Good thing I don't play any of those! If you do, you'll have to stick with Windows.
<br><br>
If those types of games aren't your cup of tea, and you're not afraid of potentially tinkering, you'll be right at home gaming on Linux. There are occasions where you may have to add a simple command to the startup flag for a game, but that's an easy copy+paste job. Most of the time though you can install a game, choose a version of Proton and it "just works". A website known as [ProtonDB](https://protondb.com/) is an incredibly useful resource when it comes to checking the playability of Steam games on Linux. For non-Steam games, using a search engine can usually bring adequate results. There are also launchers like [Lutris](https://lutris.net/) and [Heroic](https://heroicgameslauncher.com/) that cover EGS, GOG and the likes. For some games it'll actually be easier to set up than Windows, others may require an extra step or two.
<br><br>
Switching to a GNU/Linux distro from Windows and learning more about [free and open-source software](https://fsfe.org/freesoftware/freesoftware.en.html) led me down the path of finding FOSS games, which I've enjoyed quite a bit. Games like [Veloren](https://veloren.net/), [Minetest](https://minetest.net/) (specifically with the Mineclone2 mod), [OpenArena](https://openarena.ws/) and [Taisei](https://taisei-project.org/) have been really fun to play, and I wouldn't have found them had I not been interested in discovering more free software.
<br><br>
## Do distros matter?
I'm not gonna get into what a distro is, however I will mention this - one of the main differences between distros are package managers and the versions of software available to you. This means that gaming performance on one distro can vary slightly from another. Like I mentioned in the beginning of this article, the main thing you need to make sure of is that the distro you choose to game on provides you with the latest kernel and graphics drivers. This ensures you get the best performance out of your hardware. Most do, but typically with more "stable" focused distros software versions can potentially be outdated (with backported patches for security). I recommend [Pop!_OS](https://pop.system76.com) for those who want a "set it and forget it" distro to game on, and if you know what you're doing, [Arch Linux](https://archlinux.org) is a great choice for those looking for more granular control of their system. Obviously you can use other distros as well, those are just my personal recommendations. If you want a dedicated Steam machine, you could experiment with running [SteamOS Holo](https://github.com/HoloISO/holoiso) on a PC. It's literally just Arch pre-configured to be like SteamOS.
<br><br>
## Conclusion
If you're mainly a singleplayer gamer, pretty much all PC games should work, plus you'll have access to any emulator you can think of. There's tons of resources online to help you optimize your setup if need be. Multiplayer games will work, so long as they don't require a kernel level anti-cheat to run. The improvements being made as well as the acknowledgement from video game devs that Linux is a viable option for games, it's only getting better as time goes on. It's certainly not perfect and you might encounter issues, but they're usually minor ones.
There is still one problem with this - because Valve has been focusing on the "compatibility layer" Proton, most developers are still designing their games for Windows and not native Linux ports (though some are nowadays, it's becoming more common).
<br><br>
GLHF and Happy Hacking!

-H
<br><br>
This post was part of [Agora Road Travelogue](https://forum.agoraroad.com/index.php?threads/agora-road-travelogue-october-blogging.6113/) for the month of October.

