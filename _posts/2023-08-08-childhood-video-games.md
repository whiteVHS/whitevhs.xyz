---
layout: post
title: My Childhood Video Games
category: articles
tags: ["memories", "rant", "vidya"]
date: 08-08-2023
permalink: /articles/2023/08/08/childhood-video-games/index.html
---
Video Games were a huge part of my childhood, whether it be on my dad's computer or in my room on my Nintendo 64, if I wasn't outside with friends I was indoors playing games. This will mainly be a recollection of all the games and consoles I had growing up.
<br><br>
## Earliest memories
The first and earliest memories I have of playing video games would be when I was about 4, at my grandmother's place was a Nintendo 64. Anytime I was there I'd be glued to it. I played quite a few games on it, like Super Mario 64, Pokemon Stadium, 1080 Snowboarding, Starfox 64 and even Donkey Kong 64. But there was one game that *really* stood out to me - Ocarina of Time. It was the first game I really got into, and got quite far into as well. I remember completing the game when I was 6 (though I had to get somebody to do the Bottom of the Well and Shadow Temple segments, those were waaay too scary for me). Any and every chance I had to play OoT, I did. I'd even record me playing on VHS tapes we had so I could go back and watch it.
<br><br>
## My Dad's PC
When I was about 7 years old, my dad started letting me use his computer to mess around. I used to check out a bunch of flash games and animations on Homestar Runner, Newgrounds (mostly stuff geared towards video games) and early viral videos on YouTube (stuff from waverlyflams, LiamKyleSullivan, and others). He also had a few games installed, which I was definitely too young to be playing, but I don't think he really cared. The three games I remember playing on his PC were Unreal Tournament, American McGee's Alice and Grand Theft Auto III, all of which are M rated games. This was my first experience with games intended for an older audience. He also had an original Xbox that was modified to have a Super Nintendo emulator on it, and every SNES ROM released in the US (it was over 700 games). I remember being pretty overwhelmed with the choices but I do recall playing Super Mario World, Super Mario Brothers 3, Earthworm Jim, Beavis and Butthead, and oddly enough the Addams Family game.
<br><br>
## My Uncle's Collection
Around the same time, my uncle would often watch me while my parents were working. He had a PlayStation 1 and 2, with quite a selection of games, however I mainly played Frogger on the PSX and the Tony Hawk games, specifically THPS3, 4 and Tony Hawk's Underground on the PS2. He also had a steering wheel peripheral for his PS2 which he used for the game Driver 3 (or Driv3r). I remember messing with it a few times but could never quite get the hang of it. One driving game I did love as a kid though was Rush 2: Extreme Racing USA for the Nintendo 64. I'd play the absolute shit out of the Stunt track in that game. I don't remember ever doing a single race, I just loved doing flips. 
<br><br>
## Playstation at home
So I did have a Playstation at home, but the games I had for it weren't very good. I have memories of playing Monsters Inc. and Barbie Explorer, and not really enjoying them, but moreso tolerating them. Eventually we did get a PlayStation 2, but there weren't really any games for me to play on it. I remember sneaking out into the living room in the middle of the night to play a game called Oni. Looking back the game aged like milk.
<br><br>
## Getting my own handheld
I can't remember exactly when it was, but I must have been about 5 when I got my first handheld console - a red GameBoy Pocket. It wasn't anything too crazy, and my game collection for it stayed pretty small, but I got a good amount of use out of it. I had Pokemon Gold, which was strange for me since I was so used to the format of Stadium on the N64, the story-based RPG aspect of the mainline Pokemon games felt weird. I also had Kirby's Dream Land, which I also didn't get quite far in but I sure as hell replayed the first couple levels. Then finally, there was Star Wars. This game is just completely awful. I could never figure out how to progress in it, so I'd kinda wander around aimlessly until I died or got bored.
<br><br>
## Getting an upgrade
A couple years after getting my GameBoy Pocket, my parents got me a black GameBoy Advance SP, along with The Legend of Zelda: The Minish Cap. This was a game changer for me, and it quickly became my new favorite console. I also eventually got the Link to the Past/Four Swords Adventures combo cartridge, and Pokemon FireRed and Sapphire. I didn't complete the Pokemon games but I finished Minish Cap multiple times as a kid. Also I couldn't even play Four Swords Adventures cause you needed to link to other GameBoys or a GameCube, which I never owned a link cable for either.
<br><br>
## REALLY upgrading.
When I was 7, I got a Nintendo GameCube for my birthday. This was the first time I had my own console, and it was a huge deal to me. I got the Legend of Zelda: The Wind Waker and the Zelda Collector's Edition along with it, so my Zelda cravings were more than satisfied. I played the absolute shit out of all those games. That was when I really got into Wind Waker, and gave Majora's Mask more of a fair chance. I vaguely remember having some demo disc for the GameCube as well. It had demos for a few different games like Viewtiful Joe, Billy Hatcher and the Giant Egg, Prince of Persia: Sands of Time and a Tom Clancy Splinter Cell game. Super random. Eventually, I ended up getting more games like Luigi's Mansion, Mario Party 7 and Pac-Man World 2. Such good times. Some of my fondest memories were around this time. I remember one night I pulled an all-nighter to beat Tony Hawk's Project 8 on PS2, and when I finished it I woke my mom up in excitement.
<br><br>
## Dual screens!
I think I was like 7 or 8 when I got a DS. Looking back I got a lot of different consoles within the span of 1-2 years. Anyway, I got a red/black DS lite with this cool Zelda Phantom Hourglass sticker pack that was kinda like a skin for the DS. It came in a lunchbox with Link on it in the "Toon Link" artstyle. A couple of my friends and relatives at the time also had a DS, so we made good use of the multiplayer and online features like Pictochat, and wireless link play. I played a bunch of New Super Mario Bros. on it, as that was *the* must-have game for the console. I only ended up having it for a couple years though. One night I got stuck on a race in Tony Hawk's Downhill Jam and snapped my DS in half. A classic case of gamer rage. Not my best decision.
<br><br>
## Music games!
Another huge genre of gaming I was into naturally was rhythm/music games. Any and everything from Guitar Hero, Rockband to even DJ Hero, I tried 'em all and was instinctually good at them. I used to play Guitar Hero 1 with my mom on our PS2. I also remember bein like 9 at my local community center just rippin Rockband 2 on Expert, any instrument. As silly as it sounds, playing those games really helped me to get better at drumming, as I didn't have a kit to practice on at home. At least that was until I ended up getting Rockband 2 for Xbox 360 as an Xmas gift one year. I played so much RB2 it got to the point where I'd hardly leave my room. I'd even use the Rockband drum kit in practice mode as a "practice kit". I put a lot of time into that game.
<br><br>

And that's pretty much it for my childhood gaming. There may have been a couple things I left out but a lot of my memories are hazy. Going back and remembering all the fun times I had gaming helped to unlock some memories I had completely forgotten otherwise. Like there's this obscure PC rhythm game that I cannot find online for the life of me. It was almost like DDR but for PC - you use the arrow keys and time your key presses to match the arrows on-screen. It featured a lot of public domain music like classical stuff. Cannot remember the name of it for the life of me, but it was a fun one. Anyway, that's my little rant about games I loved as a kid.

-H

