---
layout: post
title: MyAnimeList and Modern Web Bloat
category: articles
tags: ["anime", "privacy", "web"]
date: 2023-03-20
permalink: /articles/2023/03/20/myanimelist-and-modern-web-bloat/index.html
---
At the beginning of this month, I decided to migrate the information from MyAnimeList profile to my website. I have a couple reasons for doing so:
<br><br>
1) MyAnimeList is riddled with trackers and cookies. It ranks a 1 out of 5 on the Startpage Privacy Protection extension (which, by the way, is the LOWEST possible score). It's one of the only sites I frequent that has such a low score.
<br><br>
2) Beyond that though, it can be annoying to have to sit there and wait for the site to finish loading your list (hundreds of entries make the list load slow), when in reality, a static list like what I have now works just fine. No javascript or bloat required, and it loads in a flash. Sure, it may not be as flashy, but it functions just as well. 
<br><br>
3) I don't particularly agree with MAL's privacy policy, therefore I think it's about time I drop it in favor of maintaining my own lists. I'll still use the site as a point of reference/database, but my account will remain inactive. I'm keeping my account on the site intact for others (I have the new list linked in the profile bio).
<br><br>
You can view my [anime](/anime) and [manga](/manga) lists here. The fewer services I use that are riddled with bloat and trackers, the better.

-H
