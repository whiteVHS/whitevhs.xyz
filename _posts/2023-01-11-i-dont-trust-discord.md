---
layout: post
title: I don't trust Discord - here's why
category: articles
tags: ["discord", "privacy", "tech"]
date: 2023-01-11
permalink: /articles/2023/01/11/i-dont-trust-discord/index.html
---

Recently, I "deleted" my Discord account (they don't *really* delete it - more on that later). It threw a lot of my friends off guard even though I had been steadily trimming down my digital life by ridding myself of all other social media and Big Tech corpo dependence. I had quite a few people ask me why I chose to do so, hopefully this suffices as an explanation:
<br><br>
I'd like to start this off by saying, I wish I could trust Discord. It's the main platform my friends frequent and seems to be quite popular for online public communities. It's ease of setup and use has to be mentioned as well. The app itself however is [heavily outdated](https://theevilskeleton.gitlab.io/2022/05/29/a-letter-to-discord-for-not-supporting-the-linux-desktop.html#determining-the-electron-version-in-use) and has quite a few problems that can hinder the experience. Besides that, my main focus is on Discord's business model (or more accurately their clear lack of one), the sketchy behaviour of the app itself and the co-founder/CEO of Discord, Jason Citron's past business endeavours.
<br><br>
OpenFeint is a social platform for mobile devices that allowed for various social networking features to be integrated into select games functionality. You could add your friends, see what games they were playing, and set challenges for them to complete. OpenFeint was founded in 2009 by none other than Jason Citron. The reason I bring this up is because A) OpenFeint was later bought out by a large company (GREE, Inc.) and B) In 2011, OpenFeint was involved in a class action lawsuit with allegations including "computer fraud, invasion of privacy, breach of contract, bad faith and seven other statutory violations." According to a news report: "OpenFeint's business plan included accessing and disclosing personal information without authorization to mobile-device application developers, advertising networks and web-analytic vendors that market mobile applications."
<br><br>
Does that sound like something you'd want to get tangled up in? Me neither.
<br><br>
The founder of OpenFeint went on to found Discord in 2015, which is again, a social platform that is centered towards games. Like OpenFeint, Discord is more or less just waiting to be bought out by a larger company (they've already had potential deals with large companies in the past). It is also without a doubt they have access to all of the information on the platform itself, and any company that eventually comes to own them will as well - which is exactly what I'll be getting into next.
<br><br>
The centralized, unencrypted nature of the service allows for any information that flows through it to be accessed by Discord, Inc. at any moment they choose. Privacy is essentially non-existent on the platform, and at best it's a nightmare (there is privacy in a more superficial sense - you can use psuedonyms and the like to protect your identity from other users, but Discord will always try to know who you are via IP address, multi-factor, etc.). In the users privacy settings, there are superficial toggles to "tailor your experience". If you have them disabled, they are still running. It's almost like they don't care about your privacy, hmm...
<br><br>
Speaking of telemetry, the amount used in the application is bananas. Any time you hover over something, click on something, etc. it pings their servers saying "user x went here, opened this, typed this...". Later I explain how you can partially mitigate this.
When you delete a discord account, it does not delete any of the associated data with the account, and instead turns the account into a "Deleted User" and resets the avatar, which by no means anonymizes the user as you can very easily figure out who it is from their messages, and if you have Developer Mode on you can see Discord IDs are still attached to "deleted" accounts. They outright refuse to delete any data you give them. This in turn violates the GDPR, or more accurately what's known as the "right to be forgotten". In fact, [Discord was fined 800K euros in November of 2022 for violating privacy laws](https://www.cnil.fr/en/discord-inc-fined-800-000-euros). If you attempt to use a third-party service to mass delete your posts/messages, you are at risk of getting your account banned as that violates Discord's ToS (and have fun deleting all your messages individually!).
<br><br>
What really saddens me about all this is I know for example, my friends *do* care about their privacy - they don't want random people seeing every message they've posted, every little detail about them, etc. when that is *exactly* what Discord staff can do - they can access any private message, server, it doesn't matter. My friends "care" about their privacy, but not all of them care enough to use an encrypted messaging service (which in reality is one of the only ways you'll get true privacy online and even then it's not 100%, but it's the best damn thing we have.)
<br><br>
If the CEO of Discord has been caught accessing and disclosing personal info without consent before, what would stop him from doing it with this tremendously more successful application? The fact that Discord is proprietary means you have no way of checking the code for potential malware/spyware. This is no accident. Their protocol, client and restriction on third-party clients is based on ensuring they can collect your data. There could very easily be malicious code in the program. They have every incentive and all the motivation to monetize your info, why wouldn't they? If they aren't selling your user data, at the very least it should still be concerning just how much control they have over the platform and the data within it.
<br><br>
[Discord devs in the past have been caught abusing their power](https://discord.news/trust-and-scam/), by revoking a vanity URL for some community server, then hours later using said vanity URL for their own server. While this may seem like a petty thing to point out, it still illustrates that the staff have abused their position of power and control, that they have this kind of access - and therefore should not be trusted.
<br><br>
The way I see it, if a private company like Discord has the means, they're most likely doing it. Even if they aren't, I'd say it's safe to assume they are and play by that. If you do use Discord please do not post anything you wouldn't want to be public. (I keep a burner account around for friends who choose convenience over long-term privacy, and by all means that is their choice, though I often nudge them into using Matrix). The same goes for any centralized, unencrypted service - they're all one breach away from the data being public. I will state this again: **DO NOT POST ANYTHING YOU WOULDN'T WANT TO BE PUBLIC**.
<br><br>
I understand not everybody cares about the information they're sharing online becoming public or easily accessible, but the fact that once your information is out there, there's no getting it back - that should be enough to help people understand why data privacy is so important.
<br><br>
If you still choose to use Discord while being aware of it's potential risks, below are a few ways you can use Discord while mitigating some of it's spookiness:
<br><br>
**Use Discord in your browser!** You will be missing some slight features like rich presence (Discord realistically doesn't need access to view every single process running on your computer anyway!) and disabling other users video in voice calls, but in return you gain much more granular control over what Discord can access including mic, camera, etc.
For mobile users this is not an option, unfortunately, so for that I have to recommend uninstalling Discord from your phone entirely.
<br><br>
**Use uBlock Origin!** You can add a filter to your uBlock Origin that will mitigate the telemetry used by Discord to collect user data. It does what the "superficial toggles" should actually do. 
Add this your uBlock Origin "My Filters" list:
<br><br>
`||discord.com/api/v*/science`
<br><br>
If you add that filter, you can see the tracking API be blocked in realtime. By the way, the old name for the "science" API was "track" but content blocking software like adblockers would deny requests to it because of it's blatant name.
<br><br>
**Disable private messages on your account and encourage others to use end-to-end-encrypted (E2EE) messaging services like Matrix or Signal!** Every little bit helps when it comes to protecting your privacy, and encouraging others to take similar action is great.
<br><br>
At the end of the day, I still use Discord but not nearly as often, and I am much more cautious of what I post or say while on the platform. I also find it quite disheartening that one must limit their usage or outright stop using a service because of how poorly it handles the users privacy - these huge public services should be more transparent and accountable for their actions. In no way do I trust the program or it's developers. You can still choose to use it any way you please, and you can even choose to trust the company. You form your own opinions on the matter, I simply wanted to state mine and present some facts along with it to support the notion that my opinion is logical. In the end, use what works for you, but more importantly - do your own research. Be aware of what you're using and how it functions. 

-H
<br><br>
![Discord? No Way!](https://whitevhs.xyz/photos/posts/discord_no-way.gif){: style="display:block;margin:auto"}
<br><br>
<sub>References:</sub>
<br><br>
<sub>OpenFeint (wikiless) | [https://wikiless.org/wiki/OpenFeint?lang=en](https://wikiless.org/wiki/OpenFeint?lang=en)</sub>
<br><br>
<sub>Discord (wikiless) | [https://wikiless.org/wiki/Discord?lang=en](https://wikiless.org/wiki/Discord?lang=en)</sub>
<br><br>
<sub>uBlock Origin filter for Discord | [https://luna.gitlab.io/discord-unofficial-docs/science.html](https://luna.gitlab.io/discord-unofficial-docs/science.html)</sub>
<br><br>
<sub>Reasons not to use Discord | [https://stallman.org/discord.html](https://stallman.org/discord.html)</sub>
<br><br>
<sub>Spyware Watchdog - Discord | [https://spyware.neocities.org/articles/discord](https://spyware.neocities.org/articles/discord)</sub>
