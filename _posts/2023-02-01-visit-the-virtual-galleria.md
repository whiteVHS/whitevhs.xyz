---
layout: post
title: Visit the Virtual Galleria
category: articles
tag: music
date: 2023-02-01
permalink: /articles/2023/02/01/visit-the-virtual-galleria/index.html
---

Join us, and visit the [virtual galleria](https://www.virtualgalleria.net).
<br><br>
![Fujisaki](https://whitevhs.xyz/photos/posts/fujisaki.gif){: style="display:block;margin:auto;"}
<br><br>
Shh... It's a secret to everybody.

-H
