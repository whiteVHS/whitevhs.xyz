---
layout: post
title: Benefits of Email Aliasing
category: articles
tags: ["email", "privacy", "security", "tech"]
date: 2023-04-02
permalink: /articles/2023/04/02/benefits-of-email-aliasing/index.html
---

Have you ever been in a situation where a temporary email address would come in handy? Or just want to keep your primary email address private? Instead of going through the hassle of making an entirely new email account, you can use email aliasing services like [SimpleLogin](https://simplelogin.io) or [AnonAddy](https://anonaddy.com). These kinds of services allow you to create "burner" or aliased email addresses on the fly; any emails sent to them get forwarded to your main inbox, and you can even send emails out as an aliased address. When you no longer need an alias, you can disable or delete them. If you own a domain, you can use it for aliases which is awesome for personal use - if you're creating an aliased address for anonymity, it's recommended to use a default domain.
<br><br>
Aliased email addresses can potentially be useful for preventing your accounts from being cracked. Similar to using randomly generated passwords with a [password manager](/articles/2023/03/25/password-manager), if you use a unique aliased email for each account, it will be much more difficult for others to gain access to all of your accounts if one is compromised because the credentials won't be consistent; they will only have access to that one account, as opposed to them having the login credentials to every account in your name. They wouldn't know your real email address as well, which prevents them from potentially gaining access to that. If you're wondering how you'd remember all these different emails and passwords - you don't! That's what a [password manager](/articles/2023/03/25/password-manager) is for.
<br><br>
Stay safe and secure out there.

-H
