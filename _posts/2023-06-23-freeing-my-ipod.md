---
layout: post
title: Freeing my iPod with Rockbox
category: articles
tags: ["foss", "music", "tech"]
date: 2023-06-23
permalink: /articles/2023/06/23/freeing-my-ipod/index.html
---

A few years ago, I purchased a 120GB iPod Classic from eBay. I used it for a bit, but eventually I overhauled my music library to only contain FLAC and OGG files (we only want them free and open formats baby!). This inevitably led to me abandoning the iPod and leaving it to collect dust. That was until recently, when I came across a forum post that mentioned [Rockbox](https://rockbox.org/) - a self-described "open-source jukebox firmware". It's available to install on a multitude of devices, not just exclusively Apple devices. A big reason for why you'd want to use this is to rid yourself of the artifical restriction and limitations of the proprietary firmware that comes preinstalled on your device. For example, by default my iPod Classic did not support playing FLAC or OGG files, but thanks to the Rockbox firmware I am able to carry my music library around in my pocket again. You don't need to sync to one device either - you can connect to any PC and just drag-and-drop your files.
<br><br>
Another huge plus - [THEMES!](https://themes.rockbox.org/index.php?target=ipodvideo)
<br><br>
The amount of themes available for this thing is insane, and you can use separate themes for the base UI and the Now Playing screen. Pretty sweet! You can also choose which font you'd like to use (I'm using one of my favorite fonts, Terminus). All in all, Rockbox has essentially "freed" my iPod and has allowed me to get use out of it once again. If you have a spare portable music player kicking around, check the Rockbox site to see if your device is compatible.

-H
