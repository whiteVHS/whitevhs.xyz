---
layout: post
title: Five simple shell commands
category: articles
tags: ["cli", "linux", "foss", "tech", "tutorial"]
date: 2023-03-02
permalink: /articles/2023/03/02/five-simple-shell-commands/index.html
---
In just about any Unix-like environment these core utilities will be made available to you through the terminal. The five commands I'm briefly going over today are some of the best to know for making your way around a terminal. Before I start, I'd like to mention that **pressing `TAB` to autocomplete will save you tons of time typing!**
<br><br>
`ls`, `cd`, `cp`, `mv` and `rm`.
<br><br>
## ls
Typically, when you open a terminal your default location is your users home directory (~ or /home/$USER/).
If you run an `ls` inside the terminal, it will output a *list* of all the files and directories within your current directory. By default, it doesn't show hidden files/folders, so you can do an `ls -a` to see all files/directories. You can also do `ls -lh` to get a more detailed list.
<br><br>
## cd
Say you want to *change directories* while in the terminal. You would use `cd`. Running `cd` on it's own will take you back to your home directory. Say you want to enter your Documents folder from home. Just run `cd Documents/` and it will take you there. You can run `pwd` to *print working directory*. If you want to go back/up a folder, run `cd ..`
<br><br>
## cp
If there's a file or directory you want to make a copy of, you can do so in the terminal! `cp` is the command used.You can copy files to other locations:
<br><br>
`cp path/to/source_file.ext path/to/target_file.ext`
<br><br>
You can even copy entire directories recursively:
<br><br>
`cp -r path/to/source_directory path/to/target_directory`
<br><br>
Pass the -v flag to enable verbose mode, which shows files as they are copied.
<br><br>
## mv
Want to rename files in the terminal? Want to move AND rename files in one command? `mv` is what you're looking for.
It's quite simple to use: `mv picture.jpg picture_renamed.jpg`
It also works with directories without a -r flag. Verbose mode can be enabled with -v.
<br><br>
## rm
Do you want to remove files in the terminal? Thankfully there is `rm`.
Deleting files is as simple as `rm picture.jpg picture2.jpg picture3.jpg`
Say you want to delete all files of a certain type. You can use what's called globbing with the wild card "*".
For example: `rm *.zip` will remove all .zip files in your current directory.
If you want to remove directories, add the -r flag, and for verbose mode add -v.
<br><br>
This was a very simple and brief explanation of some of the most popular command-line utilites in existence. I hope this can prove to be useful to those new to the terminal. If you are new to this stuff and want to dig a little deeper, I highly suggest [GNU's Introduction to the Command Line](https://static.fsf.org/nosvn/gnu-press/source-files/books/command-line.pdf). Godspeed!


-H
