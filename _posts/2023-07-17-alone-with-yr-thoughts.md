---
layout: post
title: Alone With Your Thoughts
category: articles
tags: ["thoughts", "rant"]
date: 2023-07-17
permalink: /articles/2023/07/17/alone-with-yr-thoughts/index.html
---
Have you ever spent time doing nothing? And I mean *actually* nothing - not watching television, reading books or listening to music. The thought of doing nothing to most may seem lazy, counter-productive or purely a waste of time, however that couldn't be further from the truth. Giving yourself the time and freedom to let your mind wander, reflect, and even experience boredom can be beneficial to your creativity and your overall wellbeing. It also allows a chance for you to attempt to train or control your mind (e.g. meditation, mindfulness, etc). Then why is it that so many people struggle to be alone with their thoughts, or to "turn off" their unwanted ones?
<br><br>
A big reason why most people do not enjoy the "action of inaction" so to speak, is because as a species we innately crave stimulation, or engagement of some degree. Our present-day situation of constant stimuli and attention engineering doesn't do us any favors, either. It's no wonder that most of us can't catch a break, and when we do we're not sure how to go about it. In fact it's so bad, multiple studies have been done concluding that when given the choice between an electric shock or 15 minutes alone with their thoughts, most people chose to shock themselves. Our brains crave *any* form of engagement - even negative stimuli can be seen as better than none at all. That's why I believe it's very important to understand how to use moments of downtime to your advantage, and to allow yourself to embrace the experience, process your thoughts, regulate your emotions and be content with yourself.
<br><br>
I feel that meditation goes hand-in-hand with this subject. When I am alone with my thoughts, I often think of my mind as a "sky" and look at each thought as a "passing cloud" just floating by. I don't stay on any one thought for too long, and each new thought that enters my head is just another cloud in the sky. The ultimate goal when doing this is to have the sky be clear, and to allow your mind to truly be free. That's not to say you want to stop thoughts or "turn them off", but rather detach from them in a way that can allow you to be in the present moment and not get caught up in them. This may not work for everyone, or even be desirable to everyone, but I've done this for years and it's assisted in becoming more comfortable with myself and life in general. It's allowed me to have more control over my mind, my thoughts, and consequently, my actions. If you have not tried this method of meditation yet, I do recommend it. Any remote location in nature is particularly great for this, in my opinion.
<br><br>
Ultimately, this kind of self-discipline, this mental fortitude and level of commitment is the equivalent of physical exercise, pushing your body to its limits to help strengthen it and grow. Training the mind is equally as important. Seems pretty logical right? But getting to that desired mental state is more or less an endless journey. You'll constantly be adapting and learning about yourself. All that truly matters is that at the end of the day, you can use time that would otherwise be spent distracting your mind to harness and train it instead.

-H
