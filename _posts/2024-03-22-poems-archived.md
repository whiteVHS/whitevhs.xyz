---
layout: post
title: A Collection of Poems
date: 2024-03-22
category: articles
tag: poetry
permalink: /articles/2024/03/22/poems-archived/index.html
---

Below is a collection of poems taken from various journals. Posting here for preservation purposes.<br>
Some of these were used as lyrics, some of these have never seen the light of day 'til now.<br>
They're pretty crude and some of them aren't very good.<br>
<br><br>
## Confined | 11.02.16
Confined flesh cage<br>
Bear witness to tragedy<br>
Flee from reality<br>
Unravel yourself<br>
from yourself<br>
<br><br>
Sickening<br>
How dirty yet perfect<br>
clear to see, in plain sight<br>
(watching my life from outside)<br>
Apathy awaits you.<br>
Apathy awaits you.<br>
<br><br>
For now, joke<br>
Feel a little<br>
Extreme is dull<br>
if existence is<br>
realized<br>
<br><br><br>
## Neverending | 11.23.16
The days bring with them all the heartache of yesterday<br>
Yesterday; a hollow shell of what we once knew<br>
Vanished in time and space<br>
Like a soft, lonely ocean wave - never to be seen again.<br>
Time will never end.<br>
We will.<br>
<br><br><br>
## Anti-Earth | 12.5.16
Buried beneath my existence<br>
Lies a pure, untouched essence<br>
My core in it's rawest physical form<br>
It's thoughts encourage questioning<br>
our known universe and what lies within.<br>
<br><br>
Our individual essence is a universe<br>
within itself.<br>
ever expanding, absorbing, creating, destroying.<br>
Inhabiting our mother Earth, like a flea on a domestic pet.<br>
<br><br>
We are pests to the planet<br>
We consume at an uprecedented rate<br>
We avoid and cover our deepest issues<br>
and apathetically detoriorate the planet<br>
bit by bit<br>
<br><br>
We all stand there and watch<br>
as the world burns.<br>
But after the flames and smoke have settled<br>
and we are no longer there<br>
our core, our essence<br>
will continue to linger<br>
dispersing throughout<br>
the lonely air.<br>
<br><br><br>
## Untitled | 1.13.17
floating through fog<br>
mist seeping out from the night sky<br>
twilight arrives<br>
thickening puffs of vapour<br>
obscuring my vision<br>
combustion of the heart<br>
and I feel no more<br>
<br><br>
perched on the glaze of sunrise<br>
the early morning dew coats the earth<br>
and life is renewed once again<br>
clear skies fear lies<br>
combustion of the heart<br>
and I feel forever more<br>
<br><br><br>
## Untitled | 2.3.17
Fresh, fickle minds<br>
intrigued and benign<br>
interpreting signs<br>
how divine<br>
<br><br>
cynical sinister<br>
diminish your existence<br>
engage in empathetic<br>
self-destruction<br>
<br><br>
we are a crowd of<br>
hollow vessels<br>
cities of bones<br>
the end is nigh<br>
<br><br><br>
## Untitled | 2.7.17
informed conformist<br>
appeasing society<br>
<br><br><br>
## Untitled | 2.20.17
What we percieve as the physical universe is only our idea of matter.<br>
What knowledge we have is only factual because we think it is.<br>
Our limited senses are unable to view the universe in it's truest form.<br>
<br><br><br>
## Untitled | 9.15.17
i am a tidal pool of empathy<br>
a shockwave of curiousity<br>
a seeker of authenticity<br>
a shrill laugh in a dark alley<br>
a soft breeze on a tropical beach<br>
i am the dark and the light<br>
the good and the evil<br>
and everything in between.<br>
i<br>
am me.<br>
<br><br><br>
## Untitled | 9.20.17
below hatred<br>
lies fear<br>
eyes of an enigma<br>
misery at dusk<br>
~<br>
dull and cracked eyelids<br>
seeing through dust and debris<br>
help me see anew<br>
~<br>
skulls bear the wisdom of death<br>
for no living creature hath<br>
their souls displaced, energy scattered<br>
wounds and bruises and bones to be shattered<br>
teeth to be clenched and guts to be rotted<br>
life is for living, and death be the end<br>
so we may think<br>
<br><br><br>
## Untitled | 2.xx.18
I'll travel through a tube into your magnitude<br>
I wanna be the one who knows just what to do<br>
Though I will never know quite why you're so confused<br>
So every night I look at the sky and think of you<br>
<br><br>
And you said that you'd be good for me<br>
And you said that you'd stay true to me<br>
<br><br>
So there I lie, inside my mind, dry and abused<br>
I'll bide my time abiding by the rules you choose<br>
A waste of space, this hollow face can't hide the truth<br>
So every night I look at the sky and think of you<br>
<br><br><br>
## Someday | 3.10.18
we live our lives, day by day we<br>
are living a lie, simply trapped inside<br>
our own demise, i can see it in your eyes<br>
you wanna cry and it doesn't come as a surprise<br>
the world can make you wanna die<br>
<br><br>
my love, my girl just know that<br>
everything will be alright<br>
collide and intertwine our<br>
hands and minds; your hand in mine<br>
my love, my darling you know<br>
everything will be okay<br>
although the times our rough<br>
we're bound to be happy someday<br>
<br><br><br>
## Untitled | 4.23.18
shadows on the wall<br>
they keep tellin' me to leave<br>
don't know where i'll go<br>
but where i'm headed isn't home<br>
phantoms in the hall<br>
pass me by, to no surprise<br>
i can't see at all<br>
but feel alive when i close my eyes<br>
<br><br>
and though it's pouring<br>
i can still by, i'll be alright<br>
and like the rain that's falling down<br>
we'll fall to pieces just to pick them up again<br>
<br><br>
another stormy night<br>
clouded mind that's left behind<br>
the places that it roams<br>
are left shattered like broken bones<br>


