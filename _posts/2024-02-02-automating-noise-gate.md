---
layout: post
title: How I automate using a Noise Gate with pw-link
category: articles
tags: ["bash scripts", "cli", "foss", "linux", "tech", "tutorial"]
date: 2024-02-02
permalink: /articles/2024/02/02/automating-noise-gate/index.html
---
Occasionally, I'll join a voice call with friends to play games or just catch up. When I join a call it'll use my default mic, which is fine, but I like to add EQ and a noise gate to my mic to help boost and filter it. Before, I would have to manually route everything using the `qpwgraph` GUI until recently, I discovered `pw-link`, a powerful command-line tool to control and route PipeWire audio sources; basically what `qpwgraph` does but with the advantage of being scriptable.

<br><br>
First what I did was listed the inputs and outputs available. This can be done with `pw-link -I -i` for inputs and `pw-link -I -o` for outputs. Now that I had all my audio sources listed out, I was able to write the script below:
<br><br>
{: .language-bash}
```
#!/usr/bin/env bash
# Automates disconnecting default input, routing through EQ+noise gate then routes that into Brave input

# Disconnects mic input from Brave input 
pw-link -d alsa_input.usb-PreSonus_Audio_AudioBox_USB-01.analog-stereo:capture_FL 'Brave input:input_FL';
pw-link -d alsa_input.usb-PreSonus_Audio_AudioBox_USB-01.analog-stereo:capture_FR 'Brave input:input_FR';

# Routes mic input into Carla EQ in then EQ out into Noise Gate in
pw-link alsa_input.usb-PreSonus_Audio_AudioBox_USB-01.analog-stereo:capture_FR 'x42-eq - Parametric Equalizer Mono:In';
pw-link 'x42-eq - Parametric Equalizer Mono:Out' 'Noise Gate:Input';

# Routes Noise Gate Out into Brave input
pw-link 'Noise Gate:Output' 'Brave input:input_FL';
pw-link 'Noise Gate:Output' 'Brave input:input_FR'
```
{: .language-bash}

<br><br>
First, the script disconnects my mic (front left and front right channels) from Brave, the browser I'm using for voice calls. Then it routes my mic (which is plugged into the right input of my audio interface) into an EQ I have running with a program called `carla`. The output of the EQ is then routed into a noise gate (which `carla` is also running) - it's used to stop background noise from coming through. Finally, the noise gate output is routed into the input for Brave. I have this script stored in my `$PATH`, and call it with a keybind (`Super` + `z`).

<br><br>
This took me a while to find information on how to do, simply because there doesn't seem to be much online about others doing this. The way I found a start to this solution was on the [PipeWire page on Arch Wiki](https://wiki.archlinux.org/title/PipeWire#PipeWire_patch_sets_for_command_line) - a couple scripts used `pw-link` so I looked into it (I just ran `pw-link -h`), and sure enough it was exactly what I was looking for.

<br><br>
I'm not entirely sure if this will be useful to anyone else, but I figured I'd post about it since not much else online explains this from what I can find. If you did find this useful, [let me know](mailto:contact@whitevhs.xyz)!

-H